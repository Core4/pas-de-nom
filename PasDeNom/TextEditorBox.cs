﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace PasDeNom
{
    public class TextEditorBox : RichTextBox
    {
        public bool isShrank = false;
        public int Index;
        public bool Focused = false;
        public TextEditorBox(int indx)
        {
            Index = indx;
        }

    }
}
