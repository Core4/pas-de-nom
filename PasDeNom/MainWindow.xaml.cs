﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PasDeNom;
using System.Windows.Interop;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;

namespace PasDeNomUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        List<TextEditorBox> boxes = new List<TextEditorBox>();
        // Key Map
        public static RoutedCommand RotateRightCommand = new RoutedCommand();
        public static RoutedCommand RotateLeftCommand = new RoutedCommand();

        public MainWindow()
        {
            InitializeComponent();
            SetUpUI();
            SetUpKeymap();
        }

        public void SetUpUI()
        {

            boxes.Add(CreateNewEditBox(3));

            boxes.Add(CreateNewEditBox(0));
            boxes.Add(CreateNewEditBox(1));
            boxes.Add(CreateNewEditBox(2));
            boxes[0].AppendText("Hello World");
            boxes[1].AppendText("Page 2");

            //Canvas.SetLeft(teb, 10 + margins);
            //Canvas.SetTop(teb, 10);
            Canvas.SetLeft(MainGrid, 0);
            Canvas.SetTop(MainGrid, 0);
            MainGrid.Width = getScreenWidth();

            MainGrid.Height = getScreenHeight();
            MainCanvas.Background = Brushes.AliceBlue;
            MainGrid.Background = Brushes.Aquamarine;
            
        }
        public void RotateBoxesLeft()
        {
            for (int i = 0; i < boxes.Count; i++)
            {
                if (boxes[i].Focused && boxes.Count != 1)
                {
                    if (i == 0)
                    {
                        boxes[boxes.Count - 1].Focused = true;
                    }
                    else
                    {
                        boxes[i - 1].Focused = true;
                    }
                    boxes[i].Focused = false;
                    break;
                }
            }
            RefreshBoxes();
        }
        public void RotateBoxsRight()
        {
            for (int i = 0; i < boxes.Count; i++)
            {
                if (boxes[i].Focused && boxes.Count != 1)
                {
                    if (i == boxes.Count - 1)
                    {
                        boxes[0].Focused = true;
                    }
                    else
                    {
                        boxes[i + 1].Focused = true;
                    }
                    boxes[i].Focused = false;
                    break;
                }
            }
            RefreshBoxes();
           // MessageBox.Show(boxes.Aggregate(new StringBuilder(), (x, y) => x.Append(y.Focused + " ")).ToString());

        }

        public void RefreshBoxes()
        {

            MainGrid.Children.Clear();
            for (int i = 0; i < boxes.Count(); i++)
            {
                if (boxes[i].Focused)
                {
                    if (i == 0) // left most view
                    {
                        Grid.SetColumn(boxes[boxes.Count() - 1], 0);
                        boxes[boxes.Count - 1].Index = 0;
                        MainGrid.Children.Add(boxes[boxes.Count - 1]);
                        ShrinkGridTextElement(boxes[boxes.Count - 1]);

                    }
                    else
                    {
                        Grid.SetColumn(boxes[i - 1], 0);
                        MainGrid.Children.Add(boxes[i - 1]);
                        ShrinkGridTextElement(boxes[i - 1]);
                        boxes[i - 1].Index = 0;
                    }

                    if (i == boxes.Count - 1) //right most view
                    {

                        Grid.SetColumn(boxes[0], 2);
                        MainGrid.Children.Add(boxes[0]);
                        ShrinkGridTextElement(boxes[0]);
                        boxes[i].Index = 2;
                    }
                    else
                    {
                        Grid.SetColumn(boxes[i + 1], 2);
                        MainGrid.Children.Add(boxes[i + 1]);
                        ShrinkGridTextElement(boxes[i + 1]);
                        boxes[i].Index = 2;
                    }
                    // centre view
                    Grid.SetColumn(boxes[i], 1);
                    MainGrid.Children.Add(boxes[i]);
                    UnShrinkGridTextElement(boxes[i]);
                    boxes[i].Index = 1;
                }
            }
        }

        public void ShrinkGridTextElement(TextEditorBox e)
        {
            if (e.isShrank) return;
            double initWidth = e.Width, initHeight = e.Height, initX = e.GetPoint(MainGrid).X, initY = e.GetPoint(MainGrid).Y;
            e.Width -= 40;
            e.Height -= 40;
            e.Margin = new Thickness(40, 40, 0, 0);
            e.FontSize -= 2;
            e.isShrank = true;

        }
        public void UnShrinkGridTextElement(TextEditorBox e)
        {
            if (!e.isShrank) return;
            double initWidth = e.Width, initHeight = e.Height, initX = e.GetPoint(MainGrid).X, initY = e.GetPoint(MainGrid).Y;
            e.Width += 40;
            e.Height += 40;
            e.Margin = new Thickness(20, 20, 0, 0);
            e.FontSize += 2;
            e.isShrank = false;
        }

        public TextEditorBox CreateNewEditBox(int gridIndex)
        {
            
            double screenWidth = this.GetScreen().Bounds.Width;
            double margins = (screenWidth / 3);
            TextEditorBox teb = new TextEditorBox(gridIndex);
            if (gridIndex == 1) teb.Focused = true;
            teb.VerticalAlignment = VerticalAlignment.Top;
            if (gridIndex <= 2)
            {
                Grid.SetColumn(teb, gridIndex);
            }
            Grid.SetRow(teb, 0);
            //DockPanel.SetDock(teb, (Dock)gridIndex);
            MainGrid.Children.Add(teb);
            teb.Width = margins - 40;
            teb.Height = this.GetScreen().Bounds.Height - 40;
            teb.Margin = new Thickness(20, 20, 0, 0);
            if (gridIndex != 1) { ShrinkGridTextElement(teb); }
            // DEBUG ONLY
            teb.AppendText(String.Format("{0}", gridIndex));
            teb.FontSize += 20;
            return teb;
        }
        /// <summary>
        /// http://stackoverflow.com/questions/1361350/keyboard-shortcuts-in-wpf
        /// </summary>
        private void SetUpKeymap()
        {
            RotateRightCommand.InputGestures.Add(new KeyGesture(Key.Right, ModifierKeys.Control));
        }

        public void CenterElement(Control x)
        {
            double screenWidth = getScreenWidth();
            double margins = (screenWidth / 3);
            x.Width = margins;
            x.Height = this.getScreenHeight();
        }

        private int getScreenHeight()
        {
            return this.GetScreen().Bounds.Height;
        }
        private int getScreenWidth()
        {
            return this.GetScreen().Bounds.Width;
        }

        private void CtrlRightPressed(object sender, ExecutedRoutedEventArgs e)
        {
            //MessageBox.Show("");
            RotateBoxsRight();
        }

        private void CtrlLeftPressed(object sender, ExecutedRoutedEventArgs e)
        {
            RotateBoxesLeft();
        }
    }

    static class ExtensionsForWPF
    {
        public static System.Windows.Forms.Screen GetScreen(this Window window)
        {
            return System.Windows.Forms.Screen.FromHandle(new WindowInteropHelper(window).Handle);
        }

        public static Point GetPoint(this Control c, Visual window)
        {
            return c.TransformToAncestor(window)
                          .Transform(new Point(0, 0));
        }
    }





}
