<dl>
  <dt>Authors</dt>
  <dd>Alex Pfeiffer,St John Giddy,Skye Underwood,Scott Cartwright</dd>

  <dt>Useful Links</dt>
  <dd>
    <a href="https://docs.google.com/document/d/1qazJBrDwq24mJALQ8JnByK7ZbH8HaiJObzU8UpdIoMM/edit">Rubric/Rules</a> <br/>
    <a href="https://docs.google.com/document/d/1UmU2Z973AiTvlXlx3WaFDE1ufdyj22wCRPLlzy9EsBc/edit?usp=sharing">Ideas </a> <br/>
    <a href="https://docs.google.com/document/d/1PyQhPs5f-Hgt89D8sa32RmW6aBC0YKo3UGmZ67PFjEo/edit?pli=1">Document </a> <br/>
    <a href="https://docs.google.com/drawings/d/16bBbOQR7RD6aq887W9cKK-wl2-etoKQPVXjZ4EvvagI/edit">Concept </a> <br/>
    <a href= "http://karma-runner.github.io/1.0/dev/git-commit-msg.html"> How to comment your commits <a/> <br/>
    </dd>
    
    <dt>Commit comment system to use</dt><br/> 
    <dd>
    <br/>
    Format of the commit message:<br/>
    ```
    <type>(<scope>): <subject>```<br/>
    ```
    <body>```<br/>
    ```
    <footer>```<br/>
    <br/>
    Message subject (first line)<br/> 
    The first line cannot be longer than 70 characters, the second line is always blank and other lines should be wrapped at 80 characters. The type and scope should always be lowercase as shown below. Allowed values:<br/> 
<br/> 
    feat (new feature for the user, not a new feature for build script)<br/> 
    fix (bug fix for the user, not a fix to a build script)<br/> 
    docs (changes to the documentation)<br/> 
    style (formatting, missing semi colons, etc; no production code change)<br/> 
    refactor (refactoring production code, eg. renaming a variable)<br/> 
    test (adding missing tests, refactoring tests; no production code change)<br/> 
    chore (updating grunt tasks etc; no production code change)<br/> 
<br/> 
    Example values:<br/> 
<br/> 
    init<br/> 
    runner<br/> 
    watcher<br/> 
    config<br/> 
    web-server<br/> 
    proxy<br/> 
    etc.<br/> 
<br/> 
    The can be empty (e.g. if the change is a global or difficult to assign to a single component), in which case the parentheses are omitted. In smaller projects such as Karma plugins, the is empty.<br/> 
    </dd>
</dl>